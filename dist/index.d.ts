type GuaveConfig = {
  server: string;
  id: string;
};

type GuaveInstance = {
  event: (
    name: string,
    parameters?: Record<string, string | number | boolean | undefined>
  ) => void;
  stop: () => void;
};

type GuaveView = {
  ua: string; // User agent
  ul: string | undefined; // User language
  uz: string | undefined; // User timezone
  sl: string; // Site location
  sr: string | undefined; // Site referrer
  sh: number | undefined; // Screen height
  sw: number | undefined; // Screen width
  bh: number | undefined; // Browser height
  bw: number | undefined; // Browser width
};

type GuaveCreateViewRequest = {
  d: string;
} & GuaveView;

type GuaveCreateViewResponse = {
  createView: {
    heartbeat: number | undefined;
  };
};

type GuaveHeartbeat = {
  ua: string; // User agent
  uz: string; // User timezone
};

type GuaveCreateHeartbeatRequest = {
  d: string;
} & GuaveHeartbeat;

type GuaveEvent = {
  ua: string; // User agent
  uz: string | undefined; // User timezone
  nm: string; // Name
  pm: { k: string; v: string | undefined }[]; // Parameters
};

type GuaveCreateEventRequest = {
  d: string;
} & GuaveEvent;

declare module "guave-tracker" {
  export function init(config: GuaveConfig): GuaveInstance;
}

declare global {
  interface Window {
    guave: GuaveInstance | undefined;
  }
}

export { GuaveConfig, GuaveCreateEventRequest, GuaveCreateHeartbeatRequest, GuaveCreateViewRequest, GuaveCreateViewResponse, GuaveEvent, GuaveHeartbeat, GuaveInstance, GuaveView };
