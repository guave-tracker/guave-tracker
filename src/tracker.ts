import { detect, init } from "./core/guave";

(() => {
  const config = detect();
  if (config) {
    window.guave = init(config);
  }
})();
