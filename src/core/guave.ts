import type {
  GuaveConfig,
  GuaveCreateEventRequest,
  GuaveCreateHeartbeatRequest,
  GuaveCreateViewRequest,
  GuaveCreateViewResponse,
  GuaveEvent,
  GuaveInstance,
  GuaveView,
} from "./guave.d";

/**
 * Helper function to log Guave info
 * @param message the info to log
 */
const info = (message: string) => console.info(`[Guave] ${message}`);

/**
 * Helper function to log a Guave warning
 * @param message the warning to log
 */
const warn = (message: string) => console.warn(`[Guave] ${message}`);

/**
 * Helper function to find out if the website is visible to the user
 */
const isVisible = () => document.visibilityState !== "hidden";

/**
 * Detects if there's a Guave tracking script present
 * @returns the config specified in the script tag
 */
export const detect = (): GuaveConfig | void => {
  const script = document.querySelector("[data-guave-server]");
  if (!script) return warn("Script not found");

  const server = script.getAttribute("data-guave-server");
  const id = script.getAttribute("data-guave-id");
  if (!server || !id) return warn("Missing script data");

  return {
    server,
    id,
  };
};

/**
 * Gathers all data needed and creates a view
 * @return the created view
 */
const createView = (): GuaveView => ({
  ua: window.navigator.userAgent,
  ul: window.navigator.language,
  uz: Intl.DateTimeFormat().resolvedOptions().timeZone,
  sl: window.location.href,
  sr: document.referrer,
  sh: window.screen.height,
  sw: window.screen.width,
  bh: window.innerHeight,
  bw: window.innerWidth,
});

/**
 * Creates an event object
 * @return the created event
 */
const createEvent = (
  name: string,
  parameters: Record<string, string | number | boolean | undefined> = {},
): GuaveEvent => ({
  ua: window.navigator.userAgent,
  uz: Intl.DateTimeFormat().resolvedOptions().timeZone,
  nm: name,
  pm: Object.entries(parameters).map(([key, value]) => ({
    k: key,
    v: value?.toString(),
  })),
});

/**
 * Initializes Guave tracking
 * @param config the config Guave should use for tracking
 * @returns the Guave instance
 */
export const init = (config: GuaveConfig): GuaveInstance => {
  /**
   * Sends the payload body to the Guave instance
   * @param payload the JSON payload to send
   */
  const send = <P extends Record<string, unknown>, R = never>(
    path: string,
    payload: P,
    cb?: (response: R) => void,
  ) => {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        cb?.(JSON.parse(xhr.responseText));
      }
    };
    xhr.open("POST", `${config.server}/api/${path}`);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(payload));
  };

  /**
   * Tracks current page view
   */
  const view = (cb: (heartbeat: number | undefined) => void) => {
    const view = createView();

    send<GuaveCreateViewRequest, GuaveCreateViewResponse>(
      "view",
      {
        d: config.id,
        ...view,
      },
      (response) => cb(response?.createView?.heartbeat),
    );
  };

  /**
   * Updates the current page view as a heartbeat
   */
  const heartbeat = () =>
    send<GuaveCreateHeartbeatRequest>("heartbeat", {
      d: config.id,
      ua: window.navigator.userAgent,
      uz: Intl.DateTimeFormat().resolvedOptions().timeZone,
    });

  /**
   * Tracks an event
   */
  const event = (
    name: string,
    parameters: Record<string, string | number | boolean | undefined> = {},
  ) => {
    const event = createEvent(name, parameters);

    send<GuaveCreateEventRequest>("event", {
      d: config.id,
      ...event,
    });
  };

  /**
   * Starts Guave tracking
   * @returns the created Guave instance
   */
  const start = (): GuaveInstance | void => {
    // Make sure we're running inside a browser
    if (typeof window === "undefined") return;

    // Ignore localhost
    const isLocalhost = /localhost|127\.0\.0\.1|::1|\.local|^$/i.test(
      location.hostname,
    );
    if (isLocalhost) return info("Ignoring (Reason: localhost)");

    // Ignore bots and automation software
    const isBot = /bot|crawler|spider|crawling/i.test(navigator.userAgent);
    const isAutomation = navigator.webdriver;
    if (isBot || isAutomation)
      return info("Ignoring (Reason: bot or automation)");

    let prevRoute: string | undefined = undefined;
    let idle: boolean = false;
    let skips: number = 0;
    let timer: ReturnType<typeof setInterval> | undefined = undefined;

    const handle = () => {
      const routeChanged = prevRoute !== window.location.href;

      // Track view when route has changed
      if (routeChanged) {
        view((heartbeat) => {
          clearInterval(timer);
          idle = false;

          if (heartbeat !== undefined && heartbeat !== null) {
            // Interval needs to be at least 5 seconds, otherwise default to 30 seconds
            timer = setInterval(handle, heartbeat >= 5000 ? heartbeat : 30000);
          }
        });

        prevRoute = window.location.href;
      }
      // Otherwise send a heartbeat signal
      else {
        // But skip the heartbeat signal if the window is currently not visible to the user
        if (isVisible() && !idle) {
          heartbeat();
          skips = 0;
        } else {
          skips += 1;
        }

        // Go to idle mode when 10 heartbeat signals were skipped
        if (skips >= 10) {
          idle = true;
        }
      }
    };

    // Listen to any route changes
    const originalPushState = window.history.pushState;
    window.history.pushState = new Proxy(window.history.pushState, {
      apply: (
        target,
        thisArg,
        args: Parameters<typeof window.history.pushState>,
      ) => {
        target.apply(thisArg, args);
        handle();
      },
    });
    window.addEventListener("popstate", handle);
    window.addEventListener("hashchange", handle);

    handle();

    const stop = () => {
      window.history.pushState = originalPushState;
      window.removeEventListener("popstate", handle);
      window.removeEventListener("hashchange", handle);

      clearInterval(timer);
    };

    return {
      event,
      stop,
    };
  };

  const instance = start();

  // Return dummy when instance could not be created
  if (!instance) {
    return {
      stop: () => null,
      event: () => null,
    };
  }

  return instance;
};
