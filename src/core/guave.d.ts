export type GuaveConfig = {
  server: string;
  id: string;
};

export type GuaveInstance = {
  event: (
    name: string,
    parameters?: Record<string, string | number | boolean | undefined>,
  ) => void;
  stop: () => void;
};

export type GuaveView = {
  ua: string; // User agent
  ul: string | undefined; // User language
  uz: string | undefined; // User timezone
  sl: string; // Site location
  sr: string | undefined; // Site referrer
  sh: number | undefined; // Screen height
  sw: number | undefined; // Screen width
  bh: number | undefined; // Browser height
  bw: number | undefined; // Browser width
};

export type GuaveCreateViewRequest = {
  d: string;
} & GuaveView;

export type GuaveCreateViewResponse = {
  createView: {
    heartbeat: number | undefined;
  };
};

export type GuaveHeartbeat = {
  ua: string; // User agent
  uz: string; // User timezone
};

export type GuaveCreateHeartbeatRequest = {
  d: string;
} & GuaveHeartbeat;

export type GuaveEvent = {
  ua: string; // User agent
  uz: string | undefined; // User timezone
  nm: string; // Name
  pm: { k: string; v: string | undefined }[]; // Parameters
};

export type GuaveCreateEventRequest = {
  d: string;
} & GuaveEvent;

declare module "guave-tracker" {
  export function init(config: GuaveConfig): GuaveInstance;
}

declare global {
  interface Window {
    guave: GuaveInstance | undefined;
  }
}
