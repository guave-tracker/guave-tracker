import typescript from "@rollup/plugin-typescript";
import dts from "rollup-plugin-dts";
import { terser } from "rollup-plugin-terser";

const terserOptions = {
  compress: {
    arguments: true,
    booleans_as_integers: true,
    drop_console: false,
    drop_debugger: true,
    hoist_funs: true,
    hoist_props: true,
    keep_fargs: false,
    passes: 2,
    toplevel: true,
  },
  mangle: {
    eval: true,
    toplevel: true,
    safari10: true,
  },
  module: false,
};

export default [
  {
    input: "src/tracker.ts",
    output: {
      format: "cjs",
      file: "dist/tracker.js",
    },
    plugins: [
      typescript({
        target: "es5",
      }),
      terser(terserOptions),
    ],
  },
  {
    input: "src/index.ts",
    output: {
      format: "esm",
      file: "dist/index.js",
    },
    plugins: [
      typescript({
        target: "es5",
      }),
      terser(terserOptions),
    ],
  },
  {
    input: "src/core/guave.d.ts",
    output: {
      format: "esm",
      file: "dist/index.d.ts",
    },
    plugins: [dts()],
  },
];
